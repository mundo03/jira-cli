jira-cli
=========

*A Command Line Interface for Jira*


Purpose
-------

Avoid using a browser to intereact with Jira.
This project helps create tickets quickly.

The idea is to expand this project to be able to manage your workload with it:
- Query tickets
- Assign tickets
- Change Status
- Comment a ticket
- ...
- Profit

Develop
-----

Install Requirements

    $ pip install -r requirements.txt

If you'd like to run all tests for this project (*assuming you've written
some*), you would run the following command::

    $ pytest tests

Lastly, if you'd like to cut a new release of this CLI tool, and publish it to
the Python Package Index (`PyPI <https://pypi.python.org/pypi>`_), you can do so
by running::

    $ python setup.py sdist bdist_wheel
    $ twine upload dist/*

This will build both a source tarball of your CLI tool, as well as a newer wheel
build (*and this will, by default, run on all platforms*).

The ``twine upload`` command (which requires you to install the `twine
<https://pypi.python.org/pypi/twine>`_ tool) will then securely upload your
new package to PyPI so everyone in the world can use it!
